# Hackintosh



## Table of contents

1. [Hardware](#hardware)
2. [OpenCore](#opencore)
3. [Guides](#guides)
4. [Tools](#tools)
5. [Kexts](#kexts)
6. [BOOT arguments](#boot-arguments)
7. [BIOS Settings](#bios-settings)
   - [Enable](#enable)
   - [Disable](#disable)
8. [To Do's](#to-dos)



## Hardware

- Mainboard: [MSI MPG X570 GAMING PRO CARBON WIFI](https://de.msi.com/Motherboard/MPG-X570-GAMING-PRO-CARBON-WIFI/Specification)
- CPU: [AMD Ryzen 5 3600 X](https://www.amd.com/de/products/cpu/amd-ryzen-5-3600x)
- RAM: [G.Skill Trident Z Neo DDR4 3600 MHz CL18 (F4-3600C18D-16GTZN)](https://www.gskill.com/specification/165/326/1562840461/F4-3600C18D-16GTZN-Specification)
- Graphics: [Asus ROG Strix Radeon RX 5500 XT](https://www.asus.com/de/Graphics-Cards/ROG-STRIX-RX5500XT-O8G-GAMING/)
- NVMe: [Samsung 970 EVO Plus PCIe 3.0 x4 NVMe 1.3 3D-NAND](https://www.samsung.com/semiconductor/minisite/ssd/product/consumer/970evoplus/)



## OpenCore

> "OpenCore is an open-sourced tool designed to prepare a system for macOS  booting and has been architected to alleviate many of the constraints  imposed by its alternatives like Clover(these tools are sometimes  referred to as "boot loaders")" ~ [dortania @ GitHub](https://dortania.github.io/OpenCore-Desktop-Guide/)

## Guides

- [OpenCore Desktop Guide](https://dortania.github.io/OpenCore-Desktop-Guide/)
- [Hakintosh Vanilla Desktop Guide (For beginners)](https://hackintosh.gitbook.io/-r-hackintosh-vanilla-desktop-guide/)
- [Hakintosh Multiboot](https://hackintosh-multiboot.gitbook.io/hackintosh-multiboot/)
- [Updating OpenCore and MacOS](https://dortania.github.io/OpenCore-Desktop-Guide/post-install/update.html)
- [Calculating scan policys](https://www.hackintosh-forum.de/lexicon/entry/116-scan-policy-berechnen/)
- [Ryzen Hakintosh Guide from Fritz](../Ryzen Hackintosh Guide.md)

## Tools

- [neofetch](https://github.com/dylanaraps/neofetch)
- [ProperTree](https://github.com/corpnewt/ProperTree)
- [gibMacOS](https://github.com/corpnewt/gibMacOS)
- [GenSMBIOS](https://github.com/corpnewt/GenSMBIOS)
  - iMac18,3 / iMacPro1,1

## Kexts

- [Lilu.kext](https://github.com/acidanthera/Lilu/releases)

  - Other kexts will depend on Lilu (kind of an API)

- [VirtualSMC.kext](https://github.com/acidanthera/VirtualSMC)

  - Require Lilu
  - SMC (System Management Controller) emulator

- AppleMCEReporterDisabler.kext

- [WhateverGreen.kext](https://github.com/acidanthera/WhateverGreen/releases)

  - > [Lilu](https://github.com/acidanthera/Lilu) plugin providing patches to select GPUs on macOS. Requires Lilu 1.4.0 or newer.

- [AppleALC.kext](https://github.com/acidanthera/AppleALC/releases)

  - > [...] enabling native macOS HD audio for not officially supported codecs without any filesystem modifications

- SmallTreeIntel82576.kext @ [SmallTree I211-AT patch](https://github.com/khronokernel/SmallTree-I211-AT-patch/releases) / [SmallTree Intel 211-AT @ tonymacx86](https://www.tonymacx86.com/threads/how-to-build-your-own-imac-pro-successful-build-extended-guide.229353/page-109#post-1618005)



## BOOT arguments

- `boot-args` = `-v keepsyms=1 debug=0x100 agdpmod=pikera`
  - `-v` verbose mode
  
  - `keepsyms=1` 
  
    - > follow the breadcrumbs to get past the issues. keepsyms=1 This is a companion setting to debug=0x100 that tells the OS to also  print the symbols on a kernel panic. That can give some more helpful  insight as to what's causing the panic itself
  
  - `debug=0x100`
  
    - > help you identify issues, problem kexts, etc. debug=0x100 This disables macOS's watchdog which helps prevents a reboot on a kernel panic
  
  - `agdpmod=pikera`
  
    - > GPU-Specific boot-args: boot-args Description agdpmod=pikera Used for disabling boardID on Navi GPUs(RX 5000 series), without this you'll get a black screen

## BIOS Settings

### Disable

- `Overclocking\Memory Fast Boot`
- Compatibility Support Module (CSM)(**Must be off, GPU errors like `gIO` are common when this option in enabled**)
  - This will be changed in the [Enable](#enable) part

### Enable

- `Setting\Advanced\PCI Subsystem Settings\Above 4G memory/Crypto Currency mining`
  - (**This must be on, if you can't find the option then add `npci=0x2000` to boot-args. Do not have both this option and npci enabled at the same time**)
- Set `Settings\Advanced\PCI Subsystem Settings\PCI_E1 - Max Link Speed` to `[Gen3]`
  - That is needed because of the riser cable which only supports PCIe 3.0
- `Settings\Advanced\USB Configuration\XHCI Hand-off`
- Go to `Settings\Advanced\Windows OS Configuration` and do the following:
  - `BIOS UEFI/CMS Mode` to `[UEFI]`
  - `.\Secure Boot\Secure Boot` to `[Disabled]`
  - `.\Secure Boot\Secure Boot Mode` to `[Custom]`
  - `.\Secure Boot\Delete all Secure Boot variables`

## Preparing drive

`Disk Utility` \ Choose disk \ `Erase`

- Format: `APFS` (Apple File System)
- Scheme: `GUID Parition Map`

## To Do's

- [AMD Power Gadget](https://github.com/trulyspinach/SMCAMDProcessor)
- [OpenCore 0.5.8](https://github.com/acidanthera/OpenCorePkg/releases)
- Bluetooth / WiFi
- Window Management
  - https://www.spectacleapp.com/ or install with `brew cask`
- [Grub Multiboot](https://gitlab.com/lower_snake_case/grub-multiboot)
- [Update NVMe firmware](https://www.samsung.com/semiconductor/minisite/ssd/download/tools/)
- USB Mapping
- ~~Change boot order of OC~~
  - Use `ctrl` + Index to set the default boot drive
- Fix sleep



## List of references

[OpenCore Desktop Guide](https://dortania.github.io/OpenCore-Desktop-Guide/)


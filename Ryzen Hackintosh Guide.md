# Ryzen Hackintosh Guide


I used the following guides to create my own simple Version of it. So to see things in more detail go there:

1. [OpenCore Vanilla Guide](https://khronokernel-2.gitbook.io/opencore-vanilla-desktop-guide/ "OpenCore Vanilla Guide")  
2. [AMD OS X Vanilla Guide](https://vanilla.amd-osx.com "AMD OSX")  
3. [Hackintosh Forum (DE)](https://www.hackintosh-forum.de/forum/thread/34515-howto-ryzen-el-capitan-catalina/?postID=367414#post367414 "Hackintosh Forum (DE)")
4. [Snazzy Labs AMD Hackintosh Guide](https://www.youtube.com/watch?v=l_QPLl81GrY "Ryzen Guide by Snazzy Labs on YouTube")
5. [OpenCore Presenation](https://www.youtube.com/watch?v=6nByF3xW3WA "OpenCore Presenation by griven on YouTube (HCKCN19)")


For those already set up a Hackintosh this might not be very difficul. But there are some exceptions for Ryzen.

## Plist Editor

Since we will be using OpenCore for our boot-loader, we can't use the handy CloverConfigurator. It's recommended to use something like [ProperTree](https://github.com/corpnewt/ProperTree "ProperTree by corpnewt on GitGub") or any other plist editor.  
ProperTree can automatically detect your Kexts, Drivers, SSDT Tables (ACPI), Tools and sets everything for you up in your *config.plist*. Super handy. Though I would recommend you to double check your Kexts. I had Issues with it ("Can't perform Kext Summary" Kernel Panic).

## Open Core

The [OpenCore Package](https://github.com/acidanthera/OpenCorePkg "OpenCorePackage by acidanthera on GitHub") will be our boot-loader.  
From there we can boot into any OS on our machine (due to the ACPI patches I have Issues booting into Windows since OpenCore always applies the patches regardless of the OS selected). But most important: macOS.  
In this package there is the Documentation and the EFI structure. Delete all the contents of `\EFI\OC\ACPI`, `\EFI\OC\Drivers`, `\EFI\OC\Kexts` and `\EFI\OC\Tools`.  
From there we will fill it up with all the necessary things. 


## ACPI
Advanced Configuration and Power Interface is an open industry specification that defines hardware and software interfaces that enable OS-directed configuration and Power Management to enumerate and configure motherboard devices, and manage their power.


By loading an edited DSDT via the boot loader instead of the factory one, we can achieved the following:

* faking device-ids via DSDT can replace legacy/injector kexts  
* updates may disable modified or legacy/injector kexts but cannot disable DSDT patches  
* DSDT edits can enable sleep, native speed-stepping  
* extensive edits can boost the boot process


Luckily we usually don't need to do much here. Use the [SSDTTime.bat](https://github.com/corpnewt/SSDTTime "SSDTTime by corpnewt on Github") on Windows (must be run on the System itself to work nicely!) to 4. Dump DSDT and 2. FakeEC. The resulting \*.aml files must be placed in `\EFI\OC\ACPI`.

## Firmware Drivers
The firmware drivers will be placed in `\EFI\OC\Drivers`.


* [ApfsDriverLoader.efi](https://github.com/acidanthera/AppleSupportPkg/releases "ApfsDriverLoader by acidanthera on GitHub") is needed to view APFS volumes in the bootloader.
* [VboxHfs.efi](https://github.com/acidanthera/AppleSupportPkg/releases "VboxHfs by acidanthera on GitHub") is needed to view HFS volumes in the bootloader.
* [FwRuntimeServices.efi](https://github.com/acidanthera/OpenCorePkg/releases "FwRuntimeServices by acidanthrea on GitHub") patches boot.efi for NVRAM fixes and better memory management.
* VirtualSMC.efi is needed to make the VirtualSMC.kext work properly. The download link is in the **Kexts** section.
* AppleGenericInput.efi is very helpful if you are having trouble with your Keyboard in OpenCore.

## Kexts
Kexts are **K**ernel **Ext**ensions. Since your hardware isn't 100% like Apples' hardware, we use them to get our hardware to run macOS.
I linked all the necessary kexts to a GitHub repository. From the you need to compile every single one of them. From time to time, I got issues with that, so I prefer downloading precompiled ones.  
Here is a precompiled [Kext Repo](http://kexts.goldfish64.com/ "Kext Repo"). It is very up to date. And provides you with all the important Kexts.


Kexts will be placed in `\EFI\OC\Kexts`.

#### Always required (also on Intel based Hackintosh's)
* [Lilu.kext](https://github.com/acidanthera/Lilu/releases "Lilu by acidanthera on GitHub") is needed for the next two Kexts and more. It can patch kexts, processes and libraries.
* [VirtualSMC.kext](https://github.com/acidanthera/VirtualSMC/releases "VirtualSMC by acidanthera on GitHub") emulates the SMC on a real Mac
	* It comes with 4 Plug-Ins. Use *SMCProcessor.kext* and *SCMSuperIO.kext* for your desktop.
	* It's alternative is *FakeSMC*. Use it if VirtualSMC does not work for you.
* [Whatevergreen.kext](https://github.com/acidanthera/WhateverGreen/releases "Whatevergreen by acidanthera on GitHub") does GPU wizardry. Just take it and be happy :D

#### Required on Ryzen
* [NullCPUPowerManagement.kext](https://github.com/corpnewt/NullCPUPowerManagement "NullCPUPowerManagement by corpnewt on GitHub") CPU Power Management is not supported on AMD. This Kext disables it. Actually it is no longer required thanks to OpenCore 0.5.5. It has an option called DummyCPUPowerManagement and once enabled does not require the Kext anymore.
* *[VoodooHDA.kext](https://sourceforge.net/projects/voodoohda/ "VoodooHDA on SourceForge") is only needed if AppleALC (Audio) does not work. Sounds not as good as AppleALC. So try to avoid it.*

#### Ethernet

* [IntelMausiEthernet.kext](https://github.com/Mieze/IntelMausiEthernet/ "IntelMausiEthernet by Mieze on GitHub") makes your Ethernet Card work. Most of them.
* *[SmallTreeIntel82576.kext](https://drive.google.com/file/d/0B5Txx3pb7pgcOG5lSEF2VzFySWM/view "SmallTreeIntel82576 on GoogleDrive") used for Intel I211-AT cards.*
* *[AtherosE2200Ethernet.kext](https://github.com/Mieze/AtherosE2200Ethernet "AtherosE2200Ethernet by Mieze on GitHub") is the Qualcomm Atheros Killer E2200 driver for macOS.*
* *[RealtekRTL8111.kext](https://github.com/Mieze/RTL8111_driver_for_OS_X "RealtekRTL8111 by Mieze on GitHub") is the OS X open source driver for the Realtek RTL8111/8168 family.*

#### USB
* [USBInjectAll.kext](https://bitbucket.org/RehabMan/os-x-usb-inject-all/downloads/ "USBInjectAll by RehabMan on BitBucket") is needed to configure the USB ports.

#### Audio
* [AppleALC.kext](https://github.com/acidanthera/AppleALC/releases "AppleALC by acidanthera on GitHub") is used for AppleHDA patching, used for giving you onboard audio. Ryzen doesn't support Microphone input all the time.
	* If microphone input is important to you, use VoodooHDA.

## EFI Folder
We have completed the first important section of the configuration. Now you should have a completely assembled EFI Folder. The only thing missing is the config.plist.

## config.plist
Download the [sample config](https://www.hackintosh-forum.de/attachment/123477-opencore-config-plist-2020-01-03-zip/ "config.plist by ralf. on Hackintosh-Forum.de") and first open it up with ProperTree. Delete all *Add* entries in Kernel and press `Command+R` / execute `OC Snapshot`. This injects the Kexts in the correct order once the EFI Folder we configured is selected.


* For further configuration take a look at [this](https://khronokernel-2.gitbook.io/opencore-vanilla-desktop-guide/amd-config.plist/amd-config "Opencore Vanilla Desktop Guide: AMD config.plist") page to configure all the things accordingly. Alternatively you can read the documentation that you downloaded with your OpenCore Package.
* This [official guide](https://github.com/khronokernel/Opencore-Vanilla-Desktop-Guide/blob/master/AMD/AMD-USB-map.md "OpenCore Vanilla Desktop Guide by khronokernel on GitHub") will also be helpful.

#### SMBIOS
Download the tool [GenSMBIOS](https://github.com/corpnewt/GenSMBIOS "GenSMBIOS by corpnewt on GitHub") and run the (`.command` on macOS, `.bat` on Windows) GenSMBIOS file.


In here execute the instructions for  

1. Install/Update MacSerial (use `iMac18,3`)  
2. Select config.plist  
3. Generate SMBIOS

Double check in the 'config.plist' if your SMBIOS has been copied.

***

Make sure to set the correct Layout-ID for your audio to work. Here is a [list](https://github.com/acidanthera/AppleALC/wiki/Supported-codecs "Supported Codecs by acidanthera on GitHub") with all the supported codecs.  
These are the most common:

Audio Chip | Layout-ID
---|---
ALC887,S1220A | 1,7
ALC892, ALC1150 | 1
ALC1220 | 11

Put it inside of `boot-args` in your `config.plist` as follows: `alcid='layout-id'` i.e. `alcid=11`

Now that the SMBIOS is configured, you can past the `config.plist` to the `\EFI\OC` directory.

## download macOS and prepare USB installer

First download [gibMacOS](https://github.com/corpnewt/gibMacOS "gibMacOS by corpnewt on GitHub"). In there you will find two important utilities that work on macOS and Windows.

I prefer the gibMacOS method. It creates a BOOT partition that works as the EFI partition and is always mounted. This makes configuring the EFI much more simple.

#### macOS
Download the [current version of macOS](macappstores://itunes.apple.com/us/app/macos-catalina/id1466841314?mt=12 "Download macOS from the Mac AppStore") from the Mac AppStore.  
Once finished run the following command in the terminal.	
`sudo /Applications/Install\ macOS\ Catalina.app/Contents/Resources/createinstallmedia --volume /Volumes/MyVolume`  
Replace `'MyVolume'` with your 16 GB USB stick's name.


Now, mount you EFI drive with the help of [this](https://github.com/corpnewt/MountEFI "MountEFI by corpnewt on GitHub") tool and paste your EFI Folder onto drive.

#### Windows
Run `gibMacOS.bat` as an admin and download the most up to date Version of macOS

1. R. Toggle Recovery-Only  
2. Choose most up to date version  

Once it's downloaded copy it's path to the clipboard. Next open `makeInstall.bat`, select a USB stick with at least 16 GB and use option O i.e. `1O`.  
Next, you will be prompted to paste the path of the downloaded `.pgk`.


Your EFI drive is the `BOOT` drive. Here is an EFI Folder which you want to replace with your own.

## Motherboard Configuration

Plug your USB Installer into your computer and go into the UEFI

-   Settings\Advanced\USB Configuration: XHCI Hand-off = Enabled
-   Settings\Advanced\USB Configuration: Legacy USB Support = Enabled
-   Settings\Advanced\PCI Subsystem Settings: Above 4G memory = Disabled
-   Settings\Advanced\Power Management Setup: ErP Ready = Disabled
-   Settings\Advanced\Windows OS Configuration\Secure Boot: Secure Boot = Disabled
-   Settings\Advanced\Windows OS Configuration\Secure Boot: Delete all Secure Boot variables

For more information [look here](https://khronokernel-2.gitbook.io/opencore-vanilla-desktop-guide/#recommended-bios-settings "").  Try one Setting at a time to see it's consequences.

## Insert the USB stick in your machine and install macOS on your SSD

Remember to format the drive so that macOS can be installed. GUID-Partitiontable, APFS File System.

## Post Installation
In general, the first thing you want to do, is to mount the EFI Drive from your System with the help of mountEFI.command and copy the EFI Folder to it.
Restart and tackle issues.

* [This (German) post](https://www.hackintosh-forum.de/forum/thread/34515-howto-ryzen-el-capitan-catalina/?postID=367416#post367416 "") explains many common issues and post installation fixes.
* Set up [FileVault and Securtiy](https://khronokernel-2.gitbook.io/opencore-vanilla-desktop-guide/post-install/security "Fix FileVault and Securtiy by khronokernel on GitBook").
* Having issues with [iServices](https://khronokernel-2.gitbook.io/opencore-vanilla-desktop-guide/extras/iservices "Fix iServices by khronokernel on GitBook")? Under UEFI -> Generic -> ROM copy your physical Mac Adress. Just paste the string it as data without the `:`. Did it for me. 
* Use the [Hackintool](https://github.com/headkaze/Hackintool "Hackintool by headkaze on GitHub") to create a USB.kext.

I recommend you to extract all PCI devices and add them into Device Properties. If your drive shows up as external you can fix it by adding a new child `built-in` and `01` as Data. This will fix it. You can also set the layout-id for your sound card so you don't have to configure it through the NVRAM variables. And I also added the Network card.

Only sign in to iCloud once everything else is working. Otherwise your iCloud account will have a few new Macs in it...